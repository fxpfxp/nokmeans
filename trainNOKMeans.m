function NOKMeansparam = trainNOKMeans(X, NOKMeansparam)

%number of bits
nbits = NOKMeansparam.nbits;

% lambda
if isfield(NOKMeansparam, 'lambda')
    lambda=NOKMeansparam.lambda;
else
    lambda=100000;
end

%maximum iteration
if isfield(NOKMeansparam, 'Miter')
    Miter=NOKMeansparam.Miter;
else
    Miter=50;
end

%centering the training data
mu = mean(X);
X = bsxfun(@minus, X, mu);
[A]  = NOKMeans(X,nbits,lambda,Miter);
NOKMeansparam.A=A;
NOKMeansparam.mu=mu;